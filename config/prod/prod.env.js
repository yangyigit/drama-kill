/**
 * 所有环境的默认配置
 * 不要出现项目的标记
 * 所有项目应保持KEY值统一，
 * 其他的配置文件必须是当前文件的子集
 */

"use strict";

module.exports = {
  /*------------------------ API地址、域名相关 start -----------------------*/
  // api 地址
  BASE_API: '"http://www.spring-and.cn/api"',
  /*------------------------ API地址、域名相关 end -----------------------*/
};
