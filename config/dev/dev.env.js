"use strict";
const merge = require("webpack-merge");
const common = require("../prod/prod.env.js");

module.exports = merge(common, {
  /*------------------------ API地址、域名相关 start -----------------------*/
  // api 地址
  BASE_API: '"http://www.spring-and.cn/api"',
  /*------------------------ API地址、域名相关 end -----------------------*/
});
