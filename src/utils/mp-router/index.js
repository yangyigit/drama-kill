import { router } from "./router";
import routes from "@/router/routes";

router.init({
  routes,
});

export default router;
