import moment from "moment";

class Utils {
  token = "token";
  deepCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  showToast(info) {
    uni.showToast({
      title: info,
      icon: "none",
      duration: 1500,
    });
  }

  moment() {
    moment.locale("zh-cn");
    return moment;
  }

  setStorage(key, data) {
    return uni.setStorage({
      key,
      data,
    });
  }

  getStorageSync(key) {
    try {
      const data = uni.getStorageSync(key);
      if (data) {
        return data;
      }
    } catch (e) {
      return null;
    }
  }

  //存储token
  setToken(token) {
    // token 放 globalData中
    getApp().globalData.token = token;
    return this.setStorage(this.token, token);
  }

  //获取token
  getToken() {
    const token = getApp().globalData.token;
    if (token) {
      return token;
    } else {
      return this.getStorageSync(this.token);
    }
  }

  //清空token
  removeToken() {
    getApp().globalData.token = "";
    return this.setStorage(this.token, "");
  }
}

const utils = new Utils();

export default utils;
