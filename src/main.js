import Vue from "vue";
import App from "./App";
import store from "@/store";

//工具包、配置文件挂载
import uView from "uview-ui";
import router from "@/utils/mp-router";
import utils from "@/utils/utils.js";
import api from "@/api/api.js";

Vue.prototype.$store = store;
Vue.prototype.$Router = router;
Vue.prototype.$utils = utils;
Vue.prototype.$api = api;

Vue.use(uView);
Vue.config.productionTip = false;

App.mpType = "app";

const app = new Vue({
  store,
  ...App,
});

// 引入请求封装，将app参数传递到配置中
require("@/api/request.js")(app);

app.$mount();
