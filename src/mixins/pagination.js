/**
 * author:yangyi
 * des: 分页混入
 */

export default {
  data() {
    return {
      pagitionLoading: false,
      nextPage: 1,
      pageSize: 10,
      pageNo: 1,
      showNoMore: false,
      mark: "",
    };
  },
  computed: {
    loadingType() {
      return this.nextPage > 1
        ? this.pagitionLoading
          ? "loading"
          : "loadmore"
        : "nomore";
    },
  },
  methods: {
    /*-------------------上拉加载部分--------------------------------*/
    /**
     * 上拉加载
     */
    pullupLoading(mark = "getList") {
      this.mark = mark;
      //获取更多
      this.loadMore();
      this.showNoMore = true;
    },

    /**
     * 获取当前请求页码
     * @returns {number}
     */
    getPage() {
      return this.nextPage;
    },

    /**
     *  获取更多
     */
    async loadMore() {
      //判断是否在加载中
      if (this.isLoading()) {
        return;
      }
      if (this.hasMore()) {
        this.loading();
        //延迟，为了显示正在加载几个字
        await uni.$u.sleep(500);
        //开始获取数据
        this[this.mark]();
      }
    },

    /**
     * 判断是否在加载状态
     * @returns {boolean}
     */
    isLoading() {
      return !!this.pagitionLoading;
    },

    /**
     * 解锁 完成加载
     */
    unLoading() {
      this.pagitionLoading = false;
    },

    /**
     * 判断是否获取更多
     * @returns {boolean}
     */
    hasMore() {
      if (this.nextPage > 1) {
        return true;
      } else {
        return false;
      }
    },

    /**
     * 加载中 加锁
     */
    loading() {
      this.pagitionLoading = true;
    },

    /**
     * 拼接数据
     * @param list
     */
    setMoreData(listmark = "list", addList) {
      this.listmark = listmark;
      const tempList = this[this.listmark].concat(addList);
      this[this.listmark] = tempList;
    },
    /**
     * 处理分页数据
     */
    handlingPaginatedData({ total, pageSize }) {
      let nextPage = 1;
      if (pageSize * this.nextPage < total) {
        nextPage = this.nextPage + 1;
      }
      this.nextPage = nextPage;
    },
  },
};
