export default Object.assign({
  // 测试
  test: {
    path: "/pages/test/test",
  },

  // 菜单
  menu: {
    path: "/pages/test/menu/menu",
  },

  //首页
  index: {
    path: "/pages/tabBar/index/index",
  },

  //产品列表
  goods: {
    path: "/pages/tabBar/goods/goods",
  },

  //我的
  mine: {
    path: "/pages/tabBar/mine/mine",
  },

  //预约
  appointment: {
    path: "/pages/package/appointment/appointment",
  },

  //产品详情
  goodsInfo: {
    path: "/pages/package/goodsInfo/goodsInfo",
  },
});
