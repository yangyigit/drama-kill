const http = uni.$u.http;

// post请求，获取菜单
let api = {
  //用户登录
  userLogin(params, config = {}) {
    return http.post("/v1.user/login", params, config);
  },
  //获取分类
  getCate(params, config = {}) {
    return http.post("/v1.cate/getCate", params, config);
  },
  //获取商品列表
  getMallList(params, config = {}) {
    return http.post("/v1.mall/getMallList", params, config);
  },
  //获取商品详情
  getMallDetails(params, config = {}) {
    return http.post("/v1.mall/getMallDetails", params, config);
  },
  //获取推荐数据
  getRecommend(params, config = {}) {
    return http.post("/v1.mall/getRecommend", params, config);
  },
  //获取网站基本信息
  getWebInfo(params, config = {}) {
    return http.post("/v1.configuration/getWebInfo", params, config);
  },
};

export default api;
