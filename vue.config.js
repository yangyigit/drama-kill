const webpack = require("webpack");

const env = process.env;
const { NODE_ENV, UNI_PLATFORM, VUE_APP_ENV } = env;

// 配置文件
let config = require(`./config/${VUE_APP_ENV}/${VUE_APP_ENV}.env.js`);

module.exports = {
  transpileDependencies: ["uview-ui"],
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        "process.$config": config,
      }),
    ],
  },
};
